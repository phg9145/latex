\babel@toc {german}{}
\contentsline {section}{\numberline {1}Einführung}{5}%
\contentsline {subsection}{\numberline {1.1}Motivation}{5}%
\contentsline {subsection}{\numberline {1.2}Begriffliche Abgrenzungen}{6}%
\contentsline {subsection}{\numberline {1.3}Ziele von verteilten Systemen}{7}%
\contentsline {subsubsection}{\numberline {1.3.1}Arten der Transparenz}{7}%
\contentsline {subsubsection}{\numberline {1.3.2}Offenheit}{8}%
\contentsline {subsubsection}{\numberline {1.3.3}Skalierbarkeit}{8}%
\contentsline {subsection}{\numberline {1.4}Hardware-Konzepte}{9}%
\contentsline {subsection}{\numberline {1.5}Software-Konzepte}{10}%
\contentsline {subsection}{\numberline {1.6}Systemarchitekturen}{13}%
\contentsline {subsubsection}{\numberline {1.6.1}Client-Server-Modell}{15}%
\contentsline {subsubsection}{\numberline {1.6.2}Objektorientiertes Modell}{16}%
\contentsline {subsubsection}{\numberline {1.6.3}Komponentenorientiertes Modell}{17}%
\contentsline {subsubsection}{\numberline {1.6.4}Mehrschichtenarchitekturen}{17}%
\contentsline {subsubsection}{\numberline {1.6.5}Grid-Computing}{19}%
\contentsline {subsubsection}{\numberline {1.6.6}Cloud-Computing}{20}%
\contentsline {subsubsection}{\numberline {1.6.7}Dezentrale Architekturen}{20}%
\contentsline {subsubsection}{\numberline {1.6.8}Hybride Architekturen}{21}%
\contentsline {subsection}{\numberline {1.7}Grundlagen von Prozessalgebren}{22}%
\contentsline {subsection}{\numberline {1.8}Prozessalgebren}{24}%
\contentsline {section}{\numberline {2}Prozesse}{29}%
\contentsline {subsection}{\numberline {2.1}Grundlagen}{29}%
\contentsline {subsection}{\numberline {2.2}Prozesserzeugung}{30}%
\contentsline {subsection}{\numberline {2.3}Threads}{32}%
\contentsline {subsection}{\numberline {2.4}Leistungssteigerung durch Parallelisierung}{36}%
\contentsline {subsection}{\numberline {2.5}Open MP}{38}%
\contentsline {subsection}{\numberline {2.6}Code-Migration}{41}%
\contentsline {subsection}{\numberline {2.7}Software-Agenten}{42}%
\contentsline {section}{\numberline {3}Parallele Programmierung}{43}%
\contentsline {subsection}{\numberline {3.1}Motivation}{43}%
\contentsline {subsection}{\numberline {3.2}Problem des wechselseitigen Ausschlusses}{45}%
\contentsline {subsection}{\numberline {3.3}Semaphore}{50}%
\contentsline {subsection}{\numberline {3.4}Monitore}{52}%
\contentsline {subsection}{\numberline {3.5}System V Semaphore}{54}%
\contentsline {subsubsection}{\numberline {3.5.1}Erzeugungsmechanismus}{54}%
\contentsline {subsubsection}{\numberline {3.5.2}Initialisierung}{54}%
\contentsline {subsubsection}{\numberline {3.5.3}P- und V-Operationen}{54}%
\contentsline {subsection}{\numberline {3.6}Reader-Writer-Problem}{55}%
\contentsline {section}{\numberline {4}Kommunikationsmodelle}{56}%
\contentsline {subsection}{\numberline {4.1}Grundlagen}{56}%
\contentsline {subsubsection}{\numberline {4.1.1}Adressierung}{56}%
\contentsline {subsubsection}{\numberline {4.1.2}Blockierung}{57}%
\contentsline {subsubsection}{\numberline {4.1.3}Kommunikationsformen}{58}%
\contentsline {subsection}{\numberline {4.2}Geschichtete Protokolle}{60}%
\contentsline {subsection}{\numberline {4.3}Entfernter Prozeduraufruf (RPC)}{61}%
\contentsline {subsection}{\numberline {4.4}Entfernter Objektaufruf}{63}%
\contentsline {subsection}{\numberline {4.5}Nachrichtenorienterte Kommunikation}{65}%
\contentsline {subsection}{\numberline {4.6}Stream-orientierte Kommunikation}{67}%
\contentsline {section}{\numberline {5}Namen}{68}%
\contentsline {subsection}{\numberline {5.1}Lineare Namensräume}{68}%
\contentsline {subsubsection}{\numberline {5.1.1}Einfache Lokalisierungsauflösung}{68}%
\contentsline {subsubsection}{\numberline {5.1.2}Heimatgestützte Ansätze}{69}%
\contentsline {subsubsection}{\numberline {5.1.3}Verteilte Hash-Tabellen}{69}%
\contentsline {subsection}{\numberline {5.2}Hierarchische Namensräume}{72}%
\contentsline {subsection}{\numberline {5.3}Attributierte Namensräume}{73}%
\contentsline {section}{\numberline {6}Synchronisation}{74}%
\contentsline {subsection}{\numberline {6.1}Uhr-Synchronisation}{74}%
\contentsline {subsection}{\numberline {6.2}Logische Uhren}{75}%
\contentsline {subsection}{\numberline {6.3}Globaler Zustand}{77}%
\contentsline {subsection}{\numberline {6.4}Wechselseitiger Ausschluss}{79}%
\contentsline {subsubsection}{\numberline {6.4.1}Zentralisierter Algorithmus}{79}%
\contentsline {subsubsection}{\numberline {6.4.2}Token-Ring-Algorithmus}{79}%
\contentsline {subsubsection}{\numberline {6.4.3}Algorithmus nach Ricart, Agrawala}{79}%
\contentsline {subsubsection}{\numberline {6.4.4}Dezentralisierter Algorithmus nach Lin}{80}%
\contentsline {subsubsection}{\numberline {6.4.5}Vergleich der Algorithmen}{81}%
\contentsline {subsection}{\numberline {6.5}Transaktionen}{81}%
\contentsline {subsection}{\numberline {6.6}Auswahlalgorithmen}{83}%
\contentsline {subsubsection}{\numberline {6.6.1}Ringbasierte Lösung}{83}%
\contentsline {subsubsection}{\numberline {6.6.2}Bully-Algorithmus}{84}%
\contentsline {section}{\numberline {7}Konsistenz und Replikation}{86}%
\contentsline {subsection}{\numberline {7.1}Einführung}{86}%
\contentsline {subsection}{\numberline {7.2}Datenzentrische Konsistenzmodelle}{86}%
\contentsline {subsubsection}{\numberline {7.2.1}Strenge Konsistenz}{86}%
\contentsline {subsubsection}{\numberline {7.2.2}Stufenlose Konsistenz}{87}%
\contentsline {subsubsection}{\numberline {7.2.3}Sequentielle Konsistenz}{87}%
\contentsline {subsubsection}{\numberline {7.2.4}Kausale Konsistenz}{87}%
\contentsline {subsubsection}{\numberline {7.2.5}Konsistenz mit Synchronisationsvariablen}{88}%
\contentsline {subsection}{\numberline {7.3}Client-zentrische Konsistenzmodelle}{89}%
\contentsline {subsection}{\numberline {7.4}Verteilungsprotokolle}{90}%
\contentsline {subsubsection}{\numberline {7.4.1}Aktualisierungen}{90}%
\contentsline {subsubsection}{\numberline {7.4.2}Epidemische Protokolle}{90}%
\contentsline {subsubsection}{\numberline {7.4.3}Konsistenzprotokolle}{91}%
\contentsline {section}{\numberline {8}Fehlertoleranz und Sicherheit}{92}%
\contentsline {subsection}{\numberline {8.1}Fehlertoleranz (Safety)}{92}%
\contentsline {subsubsection}{\numberline {8.1.1}Motivation}{92}%
\contentsline {subsubsection}{\numberline {8.1.2}Definitionen}{93}%
\contentsline {subsubsection}{\numberline {8.1.3}Fehlermodelle}{94}%
\contentsline {subsubsection}{\numberline {8.1.4}Redundanz}{94}%
\contentsline {subsubsection}{\numberline {8.1.5}Designaspekte}{94}%
\contentsline {subsection}{\numberline {8.2}Sicherheit (Security)}{95}%
\contentsline {subsubsection}{\numberline {8.2.1}Motivation}{95}%
\contentsline {subsubsection}{\numberline {8.2.2}Überblick}{95}%
\contentsline {subsubsection}{\numberline {8.2.3}Aspekte}{95}%
\contentsline {subsubsection}{\numberline {8.2.4}Verschlüsselung}{96}%
\contentsline {subsubsection}{\numberline {8.2.5}Blockchain}{96}%
\contentsline {section}{\numberline {9}Entwicklung von Verteilten Systemen}{98}%
\contentsline {subsection}{\numberline {9.1}Mathematische Grundlagen}{98}%
\contentsline {subsection}{\numberline {9.2}Spezifikationen}{100}%
\contentsline {subsubsection}{\numberline {9.2.1}UML}{100}%
\contentsline {subsection}{\numberline {9.3}Implementierung}{101}%
\contentsline {subsubsection}{\numberline {9.3.1}C}{101}%
\contentsline {subsubsection}{\numberline {9.3.2}Java}{102}%
\contentsline {subsection}{\numberline {9.4}Debugging Verteilter Systeme}{103}%
