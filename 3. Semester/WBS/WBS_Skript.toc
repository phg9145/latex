\babel@toc {german}{}
\contentsline {section}{\numberline {1}Kapitel 1}{3}%
\contentsline {subsection}{\numberline {1.1}Ziele wissensbasierter Systeme}{3}%
\contentsline {subsection}{\numberline {1.2}Definition von KI?}{3}%
\contentsline {subsection}{\numberline {1.3}Können Maschinen denken?}{3}%
\contentsline {subsection}{\numberline {1.4}Traveling Salesman Problem}{4}%
\contentsline {subsection}{\numberline {1.5}Arbeitsdefinition}{4}%
\contentsline {section}{\numberline {2}Wissensrepräsentation}{4}%
\contentsline {subsection}{\numberline {2.1}Produktionssysteme}{6}%
\contentsline {subsubsection}{\numberline {2.1.1}Vorwärtsverkettung}{7}%
\contentsline {subsubsection}{\numberline {2.1.2}Rückwärtsverkettung}{9}%
\contentsline {subsection}{\numberline {2.2}Stoff vom 22.10.19}{11}%
\contentsline {subsubsection}{\numberline {2.2.1}Stoff}{11}%
\contentsline {subsubsection}{\numberline {2.2.2}Prädikatenlogik}{11}%
\contentsline {subsubsection}{\numberline {2.2.3}Unifikationsalgorithmus}{15}%
\contentsline {subsection}{\numberline {2.3}Programmieren in Prolog}{16}%
\contentsline {subsubsection}{\numberline {2.3.1}typische Anwendungsgebiete}{16}%
\contentsline {subsubsection}{\numberline {2.3.2}Sprachbestandteile}{16}%
\contentsline {subsubsection}{\numberline {2.3.3}Bedienung}{16}%
\contentsline {section}{\numberline {3}Kapitel 3}{18}%
\contentsline {subsection}{\numberline {3.1}Logische Beweise}{18}%
\contentsline {subsection}{\numberline {3.2}Resolutionsregel}{18}%
\contentsline {subsection}{\numberline {3.3}Widerspruch}{19}%
\contentsline {subsection}{\numberline {3.4}Resolutionsbeweise}{19}%
\contentsline {section}{\numberline {4}Graphen}{21}%
\contentsline {subsection}{\numberline {4.1}Der A*-Algorithmus}{21}%
\contentsline {section}{\numberline {5}Constraintalgorithmen}{22}%
\contentsline {subsection}{\numberline {5.1}Einführung und Motivation}{22}%
\contentsline {subsection}{\numberline {5.2}Das Constraint Satisfaction Problem}{22}%
\contentsline {subsection}{\numberline {5.3}Constraintgraphen}{22}%
\contentsline {subsection}{\numberline {5.4}Algorithmus für lokale Konsistenz}{23}%
\contentsline {subsection}{\numberline {5.5}Partielle Constraint Satisfaction (PCSP)}{26}%
\contentsline {subsection}{\numberline {5.6}Evolutionsalgorithmen}{27}%
\contentsline {section}{\numberline {6}Neuronale Netze}{28}%
\contentsline {subsection}{\numberline {6.1}Aufbau (künstlicher) neuronaler Netze}{28}%
