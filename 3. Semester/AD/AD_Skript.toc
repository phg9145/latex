\babel@toc {german}{}
\contentsline {section}{\numberline {1}Einleitung}{3}%
\contentsline {subsection}{\numberline {1.1}Algorithmus}{3}%
\contentsline {subsection}{\numberline {1.2}$\mathcal {O}$-Notation}{4}%
\contentsline {section}{\numberline {2}Reihungen und Listen}{7}%
\contentsline {subsection}{\numberline {2.1}Einfache Datenstrukturen und ihre (typischen) Operationen}{7}%
\contentsline {subsection}{\numberline {2.2}Programmierbeispiel mit verketteten Listen (Java)}{8}%
\contentsline {subsection}{\numberline {2.3}Stack, Queue, Deque}{9}%
\contentsline {subsection}{\numberline {2.4}Queue als Reihung implementiert}{10}%
\contentsline {subsection}{\numberline {2.5}Binäre Suche}{11}%
\contentsline {subsection}{\numberline {2.6}Interpolierende Suche}{12}%
\contentsline {subsection}{\numberline {2.7}Randomisierte Skiplisten}{13}%
\contentsline {section}{\numberline {3}Graphen}{14}%
\contentsline {subsection}{\numberline {3.1}Grundlegende Definitionen}{14}%
\contentsline {subsubsection}{\numberline {3.1.1}Die Small-World-Hypothese}{15}%
\contentsline {subsection}{\numberline {3.2}Implementierung}{16}%
\contentsline {subsubsection}{\numberline {3.2.1}Adjazenzmatrix}{16}%
\contentsline {subsubsection}{\numberline {3.2.2}Adjazenzlisten}{16}%
\contentsline {subsection}{\numberline {3.3}Durchwandern von Graphen (Traversierung)}{17}%
\contentsline {subsubsection}{\numberline {3.3.1}Tiefensuche (Depth First Search DFS)}{17}%
\contentsline {subsubsection}{\numberline {3.3.2}Breitensuche (Breadth First Search BFS)}{20}%
\contentsline {subsection}{\numberline {3.4}Transitive Hülle}{21}%
\contentsline {subsection}{\numberline {3.5}Kürzeste Wege}{24}%
\contentsline {subsection}{\numberline {3.6}Flussprobleme}{26}%
\contentsline {subsection}{\numberline {3.7}Minimal aufspannende Bäume (MAB)}{29}%
\contentsline {subsection}{\numberline {3.8}Paarungen (Matchings)}{30}%
\contentsline {section}{\numberline {4}Bäume}{32}%
\contentsline {subsection}{\numberline {4.1}Definitionen und Eigenschaften}{32}%
\contentsline {subsection}{\numberline {4.2}Binärbäume und n-Bäume}{33}%
\contentsline {subsubsection}{\numberline {4.2.1}Traversieren von Binärbäumen:}{33}%
\contentsline {subsubsection}{\numberline {4.2.2}Implementierung}{34}%
\contentsline {subsection}{\numberline {4.3}Heaps}{35}%
\contentsline {subsection}{\numberline {4.4}Binäre Suchbäume}{36}%
\contentsline {subsection}{\numberline {4.5}AVL-Bäume}{37}%
\contentsline {subsection}{\numberline {4.6}B-Bäume}{38}%
\contentsline {section}{\numberline {5}Hash-Tabellen (Streuspeicherung)}{39}%
\contentsline {subsection}{\numberline {5.1}Einführung}{39}%
\contentsline {subsection}{\numberline {5.2}Wahl der Hash-Funktion $h$}{39}%
\contentsline {subsection}{\numberline {5.3}Behandlung von Kollisionen}{40}%
\contentsline {subsection}{\numberline {5.4}Offenes Hashing}{40}%
\contentsline {subsection}{\numberline {5.5}Kollisionsauflösung durch Verkettung (=chaining)}{42}%
\contentsline {subsection}{\numberline {5.6}Dynamic Hashing}{42}%
\contentsline {subsection}{\numberline {5.7}Löschen in Hash-Tabellen}{43}%
