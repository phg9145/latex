// knapsack problem
// Demo for lecture of June, 28, 2007 - S. Hahndel

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAXCAP 40
#define POPULATION 1000
#define MUTATIONSRATE 20

int items[32][2] = { {2,6},{5, 2}, {10, 11},{22, 13},{1, 5}, {3, 5},
		     {13, 6}, {11, 3}, {14, 10}, {3, 5}, {4, 8},{7, 4}, 
		     {9, 3},  {8, 5},  {16, 8},  {15, 9}, {1, 4}, 
		     {3, 3},  {5, 3},  {12, 5},  {5, 3}, {1, 4}, 
		     {9, 5},  {8, 9},  {13, 8},  {13, 6}, {5, 4}, 
		     {3 , 6}, {3, 6},  {3, 12}, {6, 4},  {6, 3} };

int population[POPULATION][2];

// evaluation of the fitness of an individuum
int fitness(int a){
	int i, result = 0, capacity = 1;
	for (i=31; i>=0; i--){
	    if (a%2){
		result = result + items[i][1];
		capacity = capacity + items[i][0];
		}
	    a = a>>1;
	}
if (capacity <= MAXCAP)
	// return result * MAXCAP  / capacity;
	  return result; 
else return 0;
}

int main(){

int i, j, z, h0, h1, i0, i1, gen = 0;
int best= 1, best_ind;

int population[POPULATION][2];

//srand(time(0));	

// calculate first population and values
for (i=0; i<POPULATION; i++){
	population[i][0] = rand();
	population[i][1] = fitness(population[i][0]);
	printf("%d\n", population[i][1]);
}

while (1){
   // Sort current population
   for ( i=POPULATION-1; i>0; i--)
      for (j=0; j<i; j++){
	if (population[j][1] < population[j+1][1]){
	   // vertauschen
	   h0 = population[j][0];
	   h1 = population[j][1];
	   population[j][0] = population[j+1][0];
	   population[j][1] = population[j+1][1];
	   population[j+1][0] = h0;
	   population[j+1][1] = h1;
	   }
	}
   // population is sorted now
   if (population[0][1] > best){
       printf("%d . Generation: Fitness %d\n", gen, population[0][1]);

        best_ind = population[0][0];
	printf("%x\n",best_ind);
	for (z=31; z>=0; z--){
	    if (best_ind%2)
		printf("(%i,%i),", items[z][0],items[z][1]);
	    best_ind = best_ind>>1;
       }
       printf("\n");
       best = population[0][1];
   }

   // calculate next generation
   for (i=POPULATION / 10 ; i<POPULATION; i++){
      // choose parent indexes
      i0 = i1 = rand() % (POPULATION / 10);
      while(i0==i1)
      	i1 = rand() % (POPULATION / 10);
      // calculate new individuum from parents
      population[i][0] = (population[i0][0] & 0xFFFF0000) |  
			 (population[i1][0] & 0x0000FFFF);

      // calculate mutation
      if ((rand() % MUTATIONSRATE) == 0){
	 population[i][0] = population[i][0] ^ (0x01<<(rand()%32));
         }

      // calculate new fitness value
      population[i][1] = fitness(population[i][0]);
      } 
   gen++;	
   }
}
